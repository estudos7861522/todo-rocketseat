import React from "react";
import Checkbox from "../Checket";
import DeleteButton from "../Delete";
import { IContentProps } from "../../Mock/model";

const generalStyle: React.CSSProperties = {
  width: "45.5rem",
  minHeight: "4.5rem",
  padding: "0 var(--gap-s)",
  background: "var(--Gray-500, #262626)",
  border: "1px solid var(--Gray-400, #333)",
  borderRadius: "var(--radius)",
  boxShadow: "0px 2px 8px 0px rgba(0, 0, 0, 0.06)",

  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  marginTop: "var(--gap-s)",
};

const divContent: React.CSSProperties = {
  width: "100%",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  padding: "0.1rem",
};

export const Content = ({task, onUpdateTask, onRemoveTask }: IContentProps) => {
  const [value, setValue] = React.useState(false);

  const handleChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
    const isChecked = event.target.checked;
    setValue(isChecked);
    onUpdateTask(task.id, isChecked);
  };

  const handleDeleteTasks = () => {
     onRemoveTask(task.id);
  }

  return (
    <div style={divContent} className="not-found">
      <div style={generalStyle}>
        <Checkbox
          label={task.title}
          id={task.id}
          name={task.id}
          check={value}
          onChange={handleChange}
        />
        <DeleteButton className="deleteBotton" onClick={handleDeleteTasks} />
      </div>
    </div>
  );
};

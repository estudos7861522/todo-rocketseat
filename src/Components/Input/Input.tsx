import { IInputProps } from "../../Mock/model";

export const Input = ({ label, id, ...props }: IInputProps) => {
  return (
    <div>
      <label htmlFor={id}>{label}</label>
      <input type="text" placeholder="Adicione uma nova tarefa" {...props} />
    </div>
  );
};

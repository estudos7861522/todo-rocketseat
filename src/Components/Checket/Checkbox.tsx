import { ICheckedProps } from "../../Mock/model";

export const Checkbox = ({
  label,
  onChange,
  id,
  check,
  ...props
}: ICheckedProps) => {
  return (
    <>
      <div>
        <input
          type="checkbox"
          id={id}
          className="checkbox"
          onChange={onChange}
          {...props}
        />
        <label
          className="checkbox-label"
          style={{
            textDecoration: check ? "line-through" : "none",
            color: check ? "var(--gray-300)" : "",
          }}
          htmlFor={id}
        >
          <span>{label}</span>
        </label>
      </div>
    </>
  );
};

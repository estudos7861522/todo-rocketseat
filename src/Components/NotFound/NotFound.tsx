import React from "react";
import clipBoard from "../../assets/svg/Clipboard.svg";

const notStyles: React.CSSProperties = {
  width: "46rem",
  padding: "64px 24px",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  gap: "var(--gap)",
  borderRadius:"var(--radius)",
  borderTop:"0.6px solid var(--grey-400, #333)"
};

const sectionStyle: React.CSSProperties = {
  padding: "var(--gap-s)",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",

};

const pStyles: React.CSSProperties = {
   color:"var(--gray-300, #808080)",
   fontFamily: "var(--font)",
   fontWeight: "800",
}

const psStyles: React.CSSProperties = {
  color:"var(--gray-300, #808080)",
  fontFamily: "var(--font)",
}

export const NotFound = () => {
 


  return (
    <section style={sectionStyle} className="not-found">
      <div style={notStyles}>
        <img src={clipBoard} alt="" />
        <div>
          <p style={pStyles}>Você ainda não tem tarefas cadastradas</p>
          <p style={psStyles}>Crie tarefas e organize seus itens a fazer</p>
        </div>
      </div>
    </section>
  );
};

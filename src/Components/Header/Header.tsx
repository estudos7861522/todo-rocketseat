import Logo from '../../assets/svg/Logo.svg';

const headerStyles: React.CSSProperties = {
   width:"100%",
   minHeight:"12.5rem",
   background:"var(--gray-700, #0D0D0D)",

   display:"flex",
   justifyContent: "center",
   alignItems: "center",
}

export const Header = () => {
  return (
    <div style={headerStyles}>
       <img src={Logo} alt="logo" />
    </div>
  )
}

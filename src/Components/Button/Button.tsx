import { IButtonProps } from "../../Mock/model";
import plusIcon from "../../assets/svg/plus.svg";

export const Button = ({ children, ...props }: IButtonProps) => {
  return (
    <button {...props}>
      {children}
      <img src={plusIcon} alt="Adicionar" />
    </button>
  );
};

import { ITalksData } from "./model";
import { v1 as uuidv1 } from 'uuid';


export const tasks: ITalksData[] = [
  {
    id: uuidv1(),
    title: "Tarefa fixada em (fake.ts) para efeito de teste dos eventos, Loading, Delete e tarefa concluidas. ",
    completed: false,
  }
];

export const fetchTasks = (): Promise<{
  data: ITalksData[];
  loading: boolean;
}> => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({ data: tasks, loading: false });
    }, 2000);
  });
};

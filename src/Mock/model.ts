import React from "react";

export type ITalksData = {
  id: string ;
  title: string ;
  completed?: boolean;
};


export type ICheckedProps = React.ComponentProps<"input"> & {
  id?: string;
  label: string; 
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  check?: string | boolean;
}

export type IContentProps  = {
  task: ITalksData;
  onUpdateTask: (taskId: string, completed: boolean) => void;
  onRemoveTask: (taskId: string) => void;
}

export type IInputProps = React.ComponentProps<"input"> & {
  label?: string;
  id?: string; 
  setState?: React.Dispatch<React.SetStateAction<string>>;
}

export type IButtonProps = React.ComponentProps<"button">;
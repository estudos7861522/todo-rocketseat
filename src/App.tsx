import Header from "./Components/Header";
import { Page } from "./Page";

import "./global.css";

function App() {
  return (
    <>
      <Header/>
      <div className="container">
        <Page/>
      </div>
    </>
  );
}

export default App;

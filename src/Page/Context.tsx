import React from "react";
import { v4 as uuidv4 } from "uuid";
import Button from "../Components/Button";
import Input from "../Components/Input";
import { ITalksData } from "../Mock/model";

const divStyles: React.CSSProperties = {
  width: "46rem",
  display: "grid",
  gridTemplateColumns: "1fr auto",
  gap: "var(--gap-s)",
};

const Context = ({
  tasks,
  setState,
  setTarefas,
}: {
  tasks: ITalksData[];
  setState: React.Dispatch<React.SetStateAction<ITalksData[]>>;
  setTarefas: React.Dispatch<React.SetStateAction<number>>;
}) => {
  const [newTasks, setNewTasks] = React.useState<ITalksData[]>([]);
  const [title, setTitle] = React.useState<string>("");

  function handleCreateNewTasks(event: React.FormEvent<HTMLFormElement>) {
    event?.preventDefault();

    const newTask: ITalksData = {
      id: uuidv4(),
      title: title,
      completed: false,
    };

    setNewTasks([...newTasks, newTask]);

    setState([...tasks, newTask]);

    setTarefas(tasks.length + 1);
    
    setTitle('');
  }


  return (
    <>
      <section>
        <form onSubmit={handleCreateNewTasks}>
          <div style={divStyles}>
            <Input
              name={"task"}
              value={title}
              onChange={(event) => setTitle(event.currentTarget.value)}
              required
            />
            <Button className="buttonCriar" disabled={title.length === 0}>Criar</Button>
          </div>
        </form>
      </section>
    </>
  );
};

export default Context;

import React from "react";
import NotFound from "../Components/NotFound";
import Context from "./Context";
import { ITalksData } from "../Mock/model";
import Content from "../Components/Content";
import { fetchTasks } from "../Mock/fake";
import Loading from "../Components/Loading";

const pageStyle: React.CSSProperties = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",

  marginTop: "-2.7rem",
};

const tarefasStyles: React.CSSProperties = {
  marginTop: "4rem",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const contentStyle: React.CSSProperties = {
  width: "46rem",
  padding: "var(--gap-s)",
  display: "flex",
  justifyContent: "space-between",
};

const pStyles: React.CSSProperties = {
  color: "var(--blue,#4eabde)",
  fontFamily: "var(--font)",
  fontSize: "0.875rem",
};

const psStyles: React.CSSProperties = {
  color: "var(--purple, #8284FA)",
  fontFamily: "var(--font)",
  fontSize: "0.875rem",
};

const spanStyle: React.CSSProperties = {
  background: "var(--gray-400, #333)",
  padding: "2px 8px",
  borderRadius: "10px",
  color: "var(--gray-200, #d9d9d9)",
  fontSize: "0.775rem",
  margin: "var(--gap-s)",
};

const Page = () => {
  const [tasks, setTasks] = React.useState<ITalksData[]>([]);
  const [tarefas, setTarefas] = React.useState<number>(0);
  const [concluidas, setConcluidas] = React.useState(0);
  const [loading, setLoading] = React.useState<boolean>(true);

  React.useEffect(() => {
    async function fetchData() {
      try {
        const { data, loading } = await fetchTasks();
        //const data = response.data;
        setTasks(data);
        setTarefas(data.length);
        setLoading(loading);
        setConcluidas(handleCompletedTasks(data));
      } catch (error) {
        console.error("Erro ao buscar dados:", error);
      }
    }

    fetchData();
  }, []);


  function handleCompletedTasks(tasks: ITalksData[]) {
    const completedTasks = tasks.filter((task) => {
      return task.completed === true;
    });
    return completedTasks.length;
  }

  function updateTask(taskId: string, completed: boolean) {
    const updatedTasks = tasks.map((ts) => {
      if (ts.id == taskId) {
        return { ...ts, completed };
      }
      return ts;
    });
    setTasks(updatedTasks);
    setConcluidas(handleCompletedTasks(updatedTasks));
  }

  function removeTask(taskId: string) {
    const updatedTask = tasks.filter((task) => task.id !== taskId);
    console.log(updateTask)
    setTasks(updatedTask);
    setTarefas(updatedTask.length);
    setConcluidas(handleCompletedTasks(updatedTask));
  }

  return (
    <>
      <div style={pageStyle}>
        <Context tasks={tasks} setState ={setTasks} setTarefas={setTarefas} />
      </div>
      <div style={tarefasStyles} className="not-found">
        <div style={contentStyle}>
          <p style={pStyles}>
            Tarefas criadas <span style={spanStyle}>{tarefas}</span>
          </p>
          <p style={psStyles}>
            Concluidas <span style={spanStyle}>{concluidas}</span>
          </p>
        </div>
      </div>
      {loading ? (
        <Loading />
      ) : Array.isArray(tasks) && tasks.length === 0 ? (
        <NotFound />
      ) : (
        tasks.map((task) => (
          <Content key={task.id} task={task} onUpdateTask={updateTask} onRemoveTask={removeTask}/>
        ))
      )}
    </>
  );
};

export default Page;
